# PROGRESS TASK

## Description

A small project which extracts data from a JSON file and displays it for the user.
There are two possible solutions added as well as a filter to manage the data.

### Stack

Front-end:
- SPA - Angular 7
Git repository: https://gitlab.com/RadoslavDonchev/progress_task_radoslav_donchev

----------

# Getting started

## Installation

Run
    `git clone https://gitlab.com/RadoslavDonchev/progress_task_radoslav_donchev`

Get into the project's folder
    `cd progress_task_radoslav_donchev/task/src`

## In order to run the project
1. run `npm install` to install all dependencies
2. run `ng s -o` to open the project in a new tab in your browser

# Functionality overview

- Extract and format data from JSON file
- Display partial data on screen
- View additional project data upon click
- Filter through the data entries
- Scalable with both phones and computers

# Author

Radoslav Donchev