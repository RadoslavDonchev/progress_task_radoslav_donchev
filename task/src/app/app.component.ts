import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public smallScreen = false;

  constructor() { }

  public myFunction() {
    this.smallScreen = !this.smallScreen;
  }

  public getSmallScreenClass() {
    if (this.smallScreen === true) {
      return { responsive: true };
    }
  }


  ngOnInit() {
  }

}
