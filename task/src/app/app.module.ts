import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from './filter.pipe';
import { SolutionTwoComponent } from './solution-two/solution-two.component';
import { SolutionOneComponent } from './solution-one/solution-one.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { DatePipe } from '@angular/common';

const appRoutes: Routes = [
  { path: 'solution-one', component: SolutionOneComponent },
  { path: 'solution-two', component: SolutionTwoComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    SolutionOneComponent,
    SolutionTwoComponent,
    ProjectDetailsComponent,
    PageNotFoundComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    FormsModule,
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
