import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {
  }

  transform(projects: any, term: any, property: any): any {
    if (term === undefined) { return projects; }

    return projects.filter((project) => {
      if (property === 'publish_date') {
        return (this.datePipe.transform(project[property]) + '').toLowerCase().includes(term.toLowerCase());
      }
      return (project[property] + '').toLowerCase().includes(term.toLowerCase());
    });
  }

}
