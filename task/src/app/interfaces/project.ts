export interface Project {
  id: string;
  company_name: string;
  comments: string;
  publish_date: Date;
  contact_name: string;
  title: string;
  location_city: string;
  description: string;
}
