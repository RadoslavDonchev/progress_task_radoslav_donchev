import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../interfaces/project';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  @Input()
  selectedProject: Project;


  constructor() { }

  ngOnInit() {
  }

}
