import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Project } from '../interfaces/project';

@Component({
  selector: 'app-solution-one',
  templateUrl: './solution-one.component.html',
  styleUrls: ['./solution-one.component.css']
})
export class SolutionOneComponent implements OnInit {

  constructor(private httpService: HttpClient) { }
  arrProjects: Project[];

  ngOnInit() {
    this.httpService.get('./assets/projects.json').subscribe(
      data => {
        this.arrProjects = (data as any).projects as Project[];
        this.arrProjects.map(element => { element.publish_date = new Date((element.publish_date as any) * 1000); });
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

  toggleAccordian(event, index) {
    const element = event.target;
    element.classList.toggle('active');
    if ((this.arrProjects as any)[index].isActive) {
      (this.arrProjects as any)[index].isActive = false;
    } else {
      (this.arrProjects as any)[index].isActive = true;
    }
    const panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + 'px';
    }
  }

}
