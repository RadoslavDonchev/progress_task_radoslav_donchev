import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Project } from '../interfaces/project';

@Component({
  selector: 'app-solution-two',
  templateUrl: './solution-two.component.html',
  styleUrls: ['./solution-two.component.css']
})
export class SolutionTwoComponent implements OnInit {

  constructor(private httpService: HttpClient) { }
  arrProjects: Project[];
  selectedProject: Project;

  changeProjectSelection(project: Project) {
    this.selectedProject = project;
  }

  ngOnInit() {
    this.httpService.get('./assets/projects.json').subscribe(
      data => {
        this.arrProjects = (data as any).projects as Project[];
        this.arrProjects.map(element => { element.publish_date = new Date((element.publish_date as any) * 1000); });
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

}
